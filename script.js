// Grammar and types
// variable hoisting

//console.log(x); // undefined
let x;

// Function hoisting

ahmad(); // "NIMA"

function ahmad() {
  console.log('NIMA');
}

//baz(); // TypeError: baz is not a function

var baz = function() {
  console.log('bar2'); // this makes error
};

// -----------------------------------------------
// -----------------------------------------------

// Control flow and error handling

// The switch Statement
const day = 'friday';
switch (day) {
  case 'monday': // day === 'monday'
    console.log('Plan course structure');
    console.log('Go to coding meetup');
    break;
  case 'tuesday':
    console.log('Prepare theory videos');
    break;
  case 'wednesday':
  case 'thursday':
    console.log('Write code examples');
    break;
  case 'friday':
    console.log('Record videos');
    break;
  case 'saturday':
  case 'sunday':
    console.log('Enjoy the weekend :D');
    break;
  default:
    console.log('Not a valid day!');
}
if (day === 'monday') {
  console.log('Plan course structure');
  console.log('Go to coding meetup');
} else if (day === 'tuesday') {
  console.log('Prepare theory videos');
} else if (day === 'wednesday' || day === 'thursday') {
  console.log('Write code examples');
} else if (day === 'friday') {
  console.log('Record videos');
} else if (day === 'saturday' || day === 'sunday') {
  console.log('Enjoy the weekend :D');
} else {
  console.log('Not a valid day!');
}

// Assignment operators
let z = 10 + 5; // 15
z += 10; // z = z + 10 = 25
z *= 4; // z = z * 4 = 100
z++; // z = z + 1
z--; // z = z - 1

// Strings and Template Literals
const firstName = 'ahmad';
const job = 'teacher';
const birthYear = 1991;
const year = 2037;
const omid =
  "I'm " + firstName + ', a ' + (year - birthYear) + ' year old ' + job + '!';
console.log(omid);
const omidNew = `I'm ${firstName}, a ${year - birthYear} year old ${job}!`;

console.log(`Just a regular string...`);

// Numbers and the '+' operator

x = 'The answer is ' + 42; // "The answer is 42"
y = 42 + ' is the answer'; // "42 is the answer"

'37' - 7; // 30
'37' + 7; // "377"
